package main

import (
	"bytes"
	"flag"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io/ioutil"
	"log"
	"math"
)

type MPT struct {
	X0 float64 // X component of coordinate
	Y0 float64 // Y component of coordinate
	X  float64 // Present X value.
	Y  float64 // Present Y value.
	R  float64 // Escape radius.
	N  int64   // Current iteration.
}

var (
	img_width            *int
	img_height           *int
	mset_x_ctr           *float64
	mset_y_ctr           *float64
	zoom                 *float64
	max_iterations       *int64
	shades_per_iteration float64
	//	max_color            int64 = 255      // 8-bit
	//	max_color            int64 = 65535    // 16-bit
	max_color int64 = 16777215 // 24-bit

	// Required for M-Set/Image conversion.
	m_x float64
	b_x float64
	m_y float64
	b_y float64
)

func init() {

	img_width = flag.Int("w", 1280, "The width of the resulting image.")
	img_height = flag.Int("h", 800, "The height of the resulting image.")
	mset_x_ctr = flag.Float64("x", -0.75, "The center of the image with respect to the x-axis of the Mandelbrot Set, between -2.5 and 1.")
	mset_y_ctr = flag.Float64("y", 0, "The center of the image with respect to the y-axis of the Mandelbrot Set, between -1 and 1.")
	zoom = flag.Float64("z", 1, "Zoom factor.")
	max_iterations = flag.Int64("i", 1000, "The number of iterations to perform before considering a coordinate going to infinity.")
}

func main() {

	flag.Parse()

	// Set initial values.

	shades_per_iteration = float64(max_color) / float64(*max_iterations)

	var (
		mset_x_total float64 = 3.5
		mset_y_total float64 = 2.0
	)

	mset_x_min := *mset_x_ctr - (mset_x_total / 2)
	mset_x_max := *mset_x_ctr + (mset_x_total / 2)
	mset_y_min := *mset_y_ctr - (mset_y_total / 2)
	mset_y_max := *mset_y_ctr + (mset_y_total / 2)

	// Set coordinates according to xoom factor.
	mset_y_total = (mset_y_max - mset_y_min) / *zoom
	mset_x_total = (mset_x_max - mset_x_min) / *zoom
	mset_x_min = *mset_x_ctr - (mset_x_total / 2)
	mset_x_max = *mset_x_ctr + (mset_x_total / 2)
	mset_y_min = *mset_y_ctr - (mset_y_total / 2)
	mset_y_max = *mset_y_ctr + (mset_y_total / 2)

	mset_ratio := mset_x_total / mset_y_total
	img_ratio := float64(*img_width) / float64(*img_height)

	if mset_ratio > img_ratio {
		// The image is taller than the default mandelbrot set.
		// Therefore, the limiting dimension is the height of the mandelbrot set.
		// Don't touch the Y figures.

		// Reset the X limits.
		mset_y_total = mset_y_max - mset_y_min
		mset_x_total = mset_y_total * img_ratio
		mset_x_min = *mset_x_ctr - (mset_x_total / 2)
		mset_x_max = *mset_x_ctr + (mset_x_total / 2)

	} else {
		// The image is flatter than the default mandelbrot set.
		// Therefore, the limiting dimension is the width of the mandelbrot set.
		// Don't touch the X figures.

		// Reset the Y limits.
		mset_x_total = mset_x_max - mset_x_min
		mset_y_total = mset_x_total / img_ratio
		mset_y_min = *mset_y_ctr - (mset_y_total / 2)
		mset_y_max = *mset_y_ctr + (mset_y_total / 2)
	}

	// Set slope of conversion lines.
	// m = rise/run
	m_x = mset_x_total / float64(*img_width)
	m_y = mset_y_total / float64(*img_height)
	b_x = mset_x_min
	b_y = mset_y_min

	img := image.NewRGBA(image.Rect(0, 0, *img_width, *img_height))
	//	img := image.NewGray16(image.Rect(0, 0, *img_width, *img_height))
	//	img := image.NewGray(image.Rect(0, 0, *img_width, *img_height))

	var (
		c     int64
		c_max int64 = 0
		clr   color.RGBA
		//				clr color.Gray16
		//		clr color.Gray
		//		i     int64
		i_max int64 = 0
		mpt   MPT
	)

	// mpt = new(MPT)

	for x := 0; x < *img_width; x++ {
		log.Printf("Percent Complete: %.1f", float64(x)/float64(*img_width)*100)
		for y := 0; y < *img_height; y++ {

			x_, y_ := getCoord(x, y)

			mpt.X0 = x_
			mpt.Y0 = y_
			mpt.X = 0
			mpt.Y = 0
			mpt.N = 0

			mpt.iterate()

			factor_iterations := float64(mpt.N) / float64(*max_iterations) // This is a decimal revealing the scale based on iterations.
			factor_radius := math.Log2(mpt.R)/2

			c = int64((factor_iterations + factor_radius) * float64(*max_iterations) * shades_per_iteration)

			// c = max_color - c

			clr.G = uint8(c)
			clr.B = uint8(c >> 8)
			clr.R = uint8(c >> 16)
			clr.A = 255

			//			log.Printf("C: %b\n", c)
			//			log.Printf("R: %b\n", color.R)
			//			log.Printf("G: %b\n", color.G)
			//			log.Printf("B: %b\n", color.B)

			//			clr.Y = uint8(c)

			// The golange image is inverted on the y-axis compared to typical cartesian systems.
			img.Set(x, *img_height-y, clr)

		}
	}

	log.Printf("img_width: %d\n", *img_width)
	log.Printf("img_height: %d\n", *img_height)
	log.Printf("mset_x_min: %0.2f\n", mset_x_min)
	log.Printf("mset_x_max: %0.2f\n", mset_x_max)
	log.Printf("mset_x_total: %0.2f\n", mset_x_total)
	log.Printf("mset_y_min: %0.2f\n", mset_y_min)
	log.Printf("mset_y_max: %0.2f\n", mset_y_max)
	log.Printf("mset_y_total: %0.2f\n", mset_y_total)
	log.Printf("m_x: %0.8f\n", m_x)
	log.Printf("m_y: %0.8f\n", m_y)

	log.Printf("i_max: %d\n", i_max)
	log.Printf("c_max: %d\n", c_max)

	log.Println("Encoding image.")
	buffer := new(bytes.Buffer)
	if err := png.Encode(buffer, img); err != nil {
		log.Fatal(err.Error())
	}
	log.Println("Writing image.")
	if err := ioutil.WriteFile(fmt.Sprintf("/Users/jackman/Pictures/mandelbrot(%.4f,%.4f)z%.1f[%dx%dz]x%d.png", *mset_x_ctr, *mset_y_ctr, *zoom, *img_width, *img_height, *max_iterations), buffer.Bytes(), 0644); err != nil {
		log.Fatal(err.Error())
	}
}

func getCoord(x int, y int) (float64, float64) {

	// -2.5 < x < 1
	//   -1 < y < 1
	// Ratio: 7:4

	// 7000 x 4000

	var (
		x_ float64
		y_ float64
	)

	x_ = float64(x)*m_x + b_x
	y_ = float64(y)*m_y + b_y

	return x_, y_
}

// z[n+1] = z[n]^2 + c;
// This function accepts a coordinate (x and y), and the current iteration.
// It returns the total number of iterations before the coordinate escapes.
// The coordinate is considered to escape when the number of iterations exceeds our color space (16-bit).
func (mpt *MPT) iterate() {

	if mpt.N >= *max_iterations {
		// calculate escape radius and return.
		mpt.R = math.Sqrt(math.Abs((mpt.X * mpt.X) + (mpt.Y * mpt.Y)))
		return
	}

	if ((mpt.X * mpt.X) + (mpt.Y * mpt.Y)) > 4 {
		//		log.Printf("Iterations: %d\n", mpt.N)

		// calculate escape radius and return.
		mpt.R = math.Sqrt(math.Abs((mpt.X * mpt.X) + (mpt.Y * mpt.Y)))
		return
	}

	mpt.N++
	x_ := (mpt.X * mpt.X) - (mpt.Y * mpt.Y) + mpt.X0
	mpt.Y = (2 * mpt.X * mpt.Y) + mpt.Y0

	mpt.X = x_

	//	log.Println(mpt.String())

	mpt.iterate()

	return
}

func (mpt *MPT) String() string {
	return fmt.Sprintf("X0: %.2f\t Y0: %.2f\t Xn: %.2f\t Yn: %.2f\t N:  %d", mpt.X0, mpt.Y0, mpt.X, mpt.Y, mpt.N)
}
